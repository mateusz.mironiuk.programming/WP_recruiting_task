import sys

import yaml

from site_explorer import SiteExplorer


def read_config(input_file: str) -> dict:
    with open(input_file) as fd:
        data = yaml.load(fd)

    config = {}
    for item in data['urls']:
        url = item['url']
        delay = item['delay']
        config[url] = delay
    return config


def main():
    config_path = sys.argv[1]
    db_path = sys.argv[2]
    config = read_config(config_path)
    explorer = SiteExplorer(config, db_path)
    explorer.run()


if __name__ == '__main__':
    main()
