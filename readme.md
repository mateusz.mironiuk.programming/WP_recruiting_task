## Overview
Repository contains 2 programs:
* site explorer
* HTTP API server

Site explorer requests websites defined in a configuration file and saves response times and status codes in a database.
It performs requests periodically with delay defined in the configuration.

API server exposes HTTP endpoints to query database with the URL statistics.


## Requirements
Site explorer requires python 3.5 or greater.
In order to run programs you need to install packages from `requirements.txt`:
```
pip install -r requirements.txt
```

## Running site explorer
In order to run the site explorer:
```
python main.py config.yml url_stats.db
```

Example `config.yml` file:
```
urls:
  - url: "http://wp.pl"
    delay: 60

  - url: "http://o2.pl"
    delay: 180

  - url: "http://pudelek.pl"
    delay: 75
```

`url_stats.db` is the name of a database file that will be created if does not exist.

## Running API server
In order to run the API server:
```
python api_server.py url_stats.db
```

`url_stats.db` is the name of a database file that will be created if does not exist.

## Endpoints
All endpoints return responses in JSON format.


### Getting all statistics
`http://localhost:5000/get_all_stats`

Example response:
```
{
  "results": [
    {
      "response_time": 1.115454,
      "status_code": 200,
      "url": "http://wp.pl"
    },
    {
      "response_time": 0.366145,
      "status_code": 200,
      "url": "http://google.com"
    }
  ]
}
```

### Getting statistics for a specific URL
`http://localhost:5000/get_stats?url=http://wp.pl`

Example response:
```
{
  "results": [
    {
      "response_time": 1.233802,
      "status_code": 200,
      "url": "http://wp.pl"
    },
    {
      "response_time": 0.366145,
      "status_code": 200,
      "url": "http://wp.pl"
    }
  ]
}
```

### Getting available URLs
`http://localhost:5000/get_urls`

Example response:
```
{
  "results": [
    "http://wp.pl",
    "http://pudelek.pl",
    "http://o2.pl"
  ]
}
```