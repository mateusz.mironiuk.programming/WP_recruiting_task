import requests
import threading
import time

from requests import RequestException

from database import DataBase, Record


class SiteExplorer:
    def __init__(self, config: dict, db_path: str):
        self.config = config
        self.db_path = db_path

    @staticmethod
    def _get_site_record(url: str) -> Record:
        response = requests.get(url)
        record = Record(
            url=url,
            response_time=response.elapsed.total_seconds(),
            status_code=response.status_code
        )
        print('I am recently requesting {}'.format(url))
        return record

    def _execute_thread(self, url: str, delay: (int, float)):
        # DataBase object requested in every thread because of issue with sqlite3 multithreading
        database = DataBase(self.db_path)
        while True:
            try:
                record = self._get_site_record(url)
            except RequestException:
                print('The Request Exception occured')
            else:
                database.save_record(record)
            time.sleep(delay)

    def run(self):
        threads = []
        for url, delay in self.config.items():
            thread = threading.Thread(name=url, target=self._execute_thread, args=(url, delay))
            threads.append(thread)

        for thread in threads:
            thread.start()
