import sqlite3
from collections import namedtuple


Record = namedtuple('Record', 'url, response_time, status_code')


class DataBase:
    def __init__(self, path: str):
        self.database = sqlite3.connect(path)
        self._create_table()

    def _create_table(self):
        cursor = self.database.cursor()
        cursor.execute('''
            CREATE TABLE
            IF NOT EXISTS sites (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            url TEXT NOT NULL,
            response_time INTEGER NOT NULL,
            status_code INTEGER NOT NULL);
        ''')

    def save_record(self, data: Record):
        """

        :param data: data to be saved
        :type data: Record
        """
        cursor = self.database.cursor()
        record = (data.url, data.response_time, data.status_code)
        cursor.execute("INSERT INTO sites(url, response_time, status_code) VALUES (?,?,?)", record)
        self.database.commit()

    def get_all_data(self) -> list[Record]:
        cursor = self.database.cursor()
        cursor.execute('SELECT url, response_time, status_code FROM sites')
        records = []
        for row in cursor:
            record = Record(
                url=row[0],
                response_time=row[1],
                status_code=row[2]
            )
            records.append(record)
        return records

    def get_data(self, url: str) -> list[Record]:
        """

        :return: list of Records
        """
        cursor = self.database.cursor()
        cursor.execute('SELECT url, response_time, status_code FROM sites WHERE url=(?)', (url,))
        records = []
        for row in cursor:
            record = Record(
                url=row[0],
                response_time=row[1],
                status_code=row[2]
            )
            records.append(record)
        return records

    def close(self):
        self.database.close()
