import sys

from flask import Flask, jsonify, request

from database import DataBase

db_path = sys.argv[1]
database = DataBase(db_path)
app = Flask(__name__)


@app.route('/get_all_stats')
def get_all_stats():
    records = database.get_all_data()
    return jsonify(to_dict(records))


@app.route('/get_stats')
def get_stats():
    url = request.args.get('url')
    records = database.get_data(url)
    return jsonify(to_dict(records))


@app.route('/get_urls')
def get_urls():
    records = database.get_all_data()
    urls = {}
    for record in records:
        urls[record.url] = 0
    result = {'results': list(urls.keys())}
    return jsonify(result)


def to_dict(records):
    list_to_json = []
    for record in records:
        line = {
            'url': record.url,
            'response_time': record.response_time,
            'status_code': record.status_code
        }
        list_to_json.append(line)
    result = {'results': list_to_json}
    return result


if __name__ == '__main__':
    app.run()
